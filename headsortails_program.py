import random

# num = random.randint(0,1)
# print(num)
# user_result = input("Podaj orzeł czy reszka\n")
#
# if (user_result == num):
#     print("udało się")
# else:
#     print("Nie udało się")

#inna wersja:

def heads_or_tails():
    num = random.randint(0, 1)
    if (num == 0):
        randomAnswer = "orzel"
    else:
        randomAnswer = "reszka"
    userAnswer = input("Orzeł czy reszka? Wpisz \"orzel\" lub \"reszka\" \n")
    if (userAnswer == "orzel" or userAnswer == "reszka"):
        if (randomAnswer == userAnswer):
            print("Brawo, udało się, wylosowano ", userAnswer, "Chcesz zagrać jeszcze raz? - y/n")
            again = input()
            if again == "y":
                heads_or_tails()
            else:
                print("Bye!")
                exit()
        else:
            print("Nie udalo sie, poprawna odpowiedz to", randomAnswer, "Chcesz zagrać jeszcze raz? - y/n")
            again = input()
            if again == "y":
                heads_or_tails()
            else:
                print("Bye!")
                exit()
    else:
        print("Nie rozpoznano odpowiedzi")
        heads_or_tails()


heads_or_tails()